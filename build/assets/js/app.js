let documentReady = function (fn) {
    if (document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        fn();
    }
}

documentReady (() => {

    // START Mobile height fix
    const changeHeight = () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    };

    changeHeight();

    window.addEventListener('resize', () => {
        changeHeight();
    });
    // END Mobile height fix

    $(".select2").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        dropdownAutoWidth: true,
        width: '100%'
    });

    document.querySelectorAll('.pretty-scroll').forEach(el => {
        new PerfectScrollbar(el,{
            wheelSpeed: 1,
            wheelPropagation: true,
            minScrollbarLength: 20
        })
    });

    document.querySelectorAll('.pretty-scroll').forEach(el => {
        new PerfectScrollbar(el,{
            wheelSpeed: 1,
            wheelPropagation: true,
            minScrollbarLength: 20
        })
    });

    $('.file-repeater, .contact-repeater, .repeater-default').repeater({
        show: function () {
            $(this).slideDown();
        },
        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        }
    });

    $('.form-media-switch').on('change', function (){
        let $this = $(this);

        $( ".form-media" ).each(function() {
            let $this = $(this);
            let $switch = $this.find('.form-media-switch');
            let $input  = $this.find('.form-media-input');

            if ($switch.is(':checked')) {
                $input.prop('disabled', false);
            }
            else {
                $input.prop('disabled', true);
            }
        });

    });
});